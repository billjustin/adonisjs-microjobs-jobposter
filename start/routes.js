'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.on('/').render('dashboard')

Route.get('/my-jobs/post-a-job', 'JobController.showForPost')
Route.post('/my-jobs/post-a-job', 'JobController.create').validator('CreateJob')

Route.get('/my-jobs', 'JobController.myJobs')
Route.get('/my-jobs/show/:id', 'JobController.show')
Route.get('/my-jobs/:id', 'JobController.showMyJobTasks')
Route.get('/my-jobs/submitted-task/show/:id', 'JobController.showTask')
Route.get('/my-jobs/approve/:id', 'JobController.taskApprove')
Route.get('/my-jobs/reject/:id', 'JobController.taskReject')
Route.get('/browse', 'JobController.browseJobs')

Route.get('/admin/submitted-jobs', 'AdminController.submittedJobs')
Route.post('/admin/submitted-jobs', 'AdminController.approveJob')
Route.get('/admin/submitted-jobs/approve/:id', 'AdminController.approveJob')
Route.get('/admin/submitted-jobs/reject/:id', 'AdminController.rejectJob')

Route.on('/pending-review').render('workers.jobs.pending-review')
Route.on('/jobdesc').render('workers.jobs.job-description')
Route.on('/profile').render('workers.profile')

Route.on('/login').render('auth.login')
Route.post('/login', 'UserController.login')

Route.on('/signup').render('auth.signup')
Route.post('/signup', 'UserController.create')