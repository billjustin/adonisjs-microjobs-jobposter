'use strict'
const Category = use('App/Models/Category')
const Country = use('App/Models/Country')
const Job = use('App/Models/Job')
const Task = use('App/Models/Task')
const User = use('App/Models/User')
const Database = use('Database')

class AdminController {
    async submittedJobs({ view }) {
        const jobs = await Job.query().where('status', 0).fetch()

        return view.render('admin.submitted-jobs', { jobs: jobs.toJSON() })
    }

    async approveJob({ params }) {
        const jobs = await Job.find(params.id)
        jobs.status = 1
        jobs.save()

        return redirect('back')
    }

    async rejectJob({ params }) {
        const jobs = await Job.find(params.id)
        jobs.status = 2
        jobs.save()

        return redirect('back')
    }
}

module.exports = AdminController
