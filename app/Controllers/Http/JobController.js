'use strict'
const Category = use('App/Models/Category')
const Country = use('App/Models/Country')
const Job = use('App/Models/Job')
const Task = use('App/Models/Task')
const User = use('App/Models/User')
const Database = use('Database')

class JobController {
    async create({ request, response, auth }) {
        // const job = await Job.create(request.except('csrf'))
        await Job.create(request.only(['title', 'countryId', 'caetgoryId', 'slots', 'price', 'description', 'requirements', 'user_id']))
        
        response.redirect('/my-jobs')
    }

    async browseJobs({ view }) {
        // const jobs = await Job.query().where('status', 1).fetch()
        const jobs = await Database.table('jobs').innerJoin('countries', 'jobs.countryId', 'countries.id')
        return view.render('workers.jobs.index', { jobs: jobs.toJSON() })
        
    }

    async showForPost({ view, auth }) {
        const categories = await Category.all()
        const countries = await Country.all()
        const user = auth.user
        return view.render('workers.myjobs.post-a-job', { categories: categories.toJSON(), countries: countries.toJSON(), user: user })
    }

    async submimttedTasksForJob({ view }) {
        const tasks = await tasks.all()

        return view.render('workers.myjobs.submitted-tasks/index', { tasks: tasks.toJSON() })
    }

    async myJobs({ view, auth }) {
        const jobs = await auth.user.jobs().fetch()
        
        return view.render('workers.myjobs.index', { jobs: jobs.toJSON() })
    }   

    async showMyJobTasks({ view, params }) {
        const tasks = await Task.query().where('jobId', params.id).fetch()
        
        return view.render('workers.myjobs.submitted-tasks.index', { tasks: tasks.toJSON() })
    }

    async showTask({ view, params }) {
        const task = await Task.find(params.id)

        return view.render('workers.myjobs.submitted-tasks.show', { task: task.toJSON() })
    }

    async taskApprove({ response, params }) {
        const task = await Task.find(params.id)

        // update task status
        task.status = 1
        await task.save()

        // join to get task rate
        const rate = await Database.query().where('tasks.id', params.id).table('tasks').innerJoin('jobs', 'jobs.id', 'tasks.jobID')
        
        const user = await User.find(rate[0].user_id)
        user.balance += rate[0].price
        user.save()
        // console.log(user.balance)
        return response.redirect('back')
    }

    async taskReject({ response, params }) {
        const task = await Task.find(params.id)

        task.status = 2
        await task.save()
        // console.log(task.toJSON())
        return response.redirect('back')
    }
}

module.exports = JobController
