'use strict'
const User = use('App/Models/User')

class UserController {
    async create({ request, respone, auth }) {
        const user = await User.create(request.only(['username', 'email', 'password', 'firstname', 'lastname']))
        // console.log(request.body)
        await auth.login(user)
        return redirect('/my-jobs')
    }

    async login({ request, response, auth, session }) {
        const { email, password } = request.all()

        try {
            await auth.attempt(email, password)
            return response.redirect('/my-jobs')
        } catch(error) {
            session.flash({loginError: 'Invalid credentials.'})
            return response.redirect('back')
        }
    }
}

module.exports = UserController
