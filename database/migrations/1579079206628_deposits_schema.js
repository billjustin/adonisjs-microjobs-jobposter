'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DepositsSchema extends Schema {
  up () {
    this.create('deposits', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('deposits')
  }
}

module.exports = DepositsSchema
