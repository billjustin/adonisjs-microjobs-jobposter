'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ComplaintsSchema extends Schema {
  up () {
    this.create('complaints', (table) => {
      table.increments()
      table.integer('taskId', 200).notNullable()
      table.string('complaint', 250).notNullable()
      table.integer('userId', 200).notNullable()
      table.integer('status', 1).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('complaints')
  }
}

module.exports = ComplaintsSchema
