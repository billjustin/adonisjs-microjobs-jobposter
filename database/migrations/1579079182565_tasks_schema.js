'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TasksSchema extends Schema {
  up () {
    this.create('tasks', (table) => {
      table.increments()
      table.integer('jobId', 150).notNullable()
      table.string('proof', 250).notNullable()
      table.integer('userId', 50).notNullable()
      table.integer('status', 1).notNullable().default(0)
      table.string('employersComment', 250)
      table.timestamps()
    })
  }

  down () {
    this.drop('tasks')
  }
}

module.exports = TasksSchema
