'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WithdrawSchema extends Schema {
  up () {
    this.create('withdraws', (table) => {
      table.increments()
      table.integer('userId', 80).notNullable()
      table.string('paypal', 80).notNullable()
      table.float('amount').notNullable()
      table.date('date').notNullable()
      table.integer('status', 1).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('withdraws')
  }
}

module.exports = WithdrawSchema
