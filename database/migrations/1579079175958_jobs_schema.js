'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class JobsSchema extends Schema {
  up () {
    this.create('jobs', (table) => {
      table.increments()
      table.string('title', 80).notNullable()
      table.string('description', 250).notNullable()
      table.string('requirements', 199).notNullable()
      table.integer('rate', 80).notNullable()
      table.integer('available', 80).notNullable().default(0)
      table.float('price').notNullable()
      table.integer('countryId', 3).notNullable()
      table.integer('categoryId', 3).notNullable()
      table.integer('user_id', 50).notNullable()
      table.integer('status', 50).notNullable().default(0)
      table.timestamps()
    })
  }

  down () {
    this.drop('jobs')
  }
}

module.exports = JobsSchema
